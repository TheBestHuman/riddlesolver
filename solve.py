from nltk.corpus import words
english_words_set = set(words.words())

import inflect
import functools

def transformer(input_value_list, guess):
    guess_vals = list(get_letter_value_list(guess))

    guess_len = len(guess_vals)
    result_vals = []
    for idx, input_val in enumerate(list(input_value_list)):
    
        guess_idx = idx % guess_len
        new_val = input_val -guess_vals[guess_idx]
        #print ([ input_val, guess_vals[guess_idx]])
        if new_val < 0:
            new_val += 26
        #print(new_val)
        result_vals.append(new_val)

    #https://stackoverflow.com/a/180615
    return ''.join(chr(i + 97) for i in result_vals)
    

def get_letter_value_list(str):
    str_letters = [char for char in str.lower()]
    return list(map((lambda c: ord(c) - 97 ), str_letters))

print("Starting up, please wait\n")
eng = inflect.engine()
plur = (lambda word:eng.plural(word))

english_words_with_plurals = english_words_set.union( map(plur, english_words_set))

input = input("Please enter a cipher: ")
input_vals = get_letter_value_list(input)
input_lambda = (lambda x: transformer(input_vals, x))

cipher_results = map(input_lambda, english_words_with_plurals)
word_results = map((lambda txt: txt if (txt in english_words_with_plurals) else ''), cipher_results)

combiner = (lambda input, output: None if (output == '') else f"{input} - {output}")

output = '\n'.join(filter(None, map(combiner, english_words_with_plurals, word_results)))
print(output)

# Use an official Python runtime as a parent image
FROM python:3.8-slim-buster

# Set the working directory in the container to /app
WORKDIR /app

# Add current directory files to /app in the container
ADD . /app

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir inflect nltk

# Download the nltk corpus of english words
RUN python -m nltk.downloader words

# Run solve.py when the container launches
CMD ["python", "solve.py"]

# Riddle Solver

A command line app to solve a cipher from a particular book of riddles. The riddle in question is:
   ```We spied her in the park
        Feeding birds with no bread.
        We spied her in the dark
        Knitting blankets for the dead.

        SGIFLEAS
   ```
<details> 
  <summary>The answer is: </summary>
   spider - arachnid
</details>
</p>


## Requirements
- Docker
- Python 3.8

## Build Instructions

1. **Clone the repository**

   Begin by cloning the repository to your local machine. 

   ```shell
   git clone <repository_url>
   cd <repository_directory>
   ```

2. **Build the Docker image**

   In the root directory of the project (the one that contains the `Dockerfile`), run the following command to build the Docker image.

   ```shell
   docker build -t python-app .
   ```

   This will build a Docker image and tag it as `python-app`.

3. **Run the Docker container**

   After building the image, you can start an interactive Docker container with the following command:

   ```shell
   docker run -it --rm python-app bash
   ```

   This starts an interactive session inside the Docker container. The `bash` command at the end starts a bash shell in the container.

4. **Run the Python script**

   Once inside the Docker container, you can run the Python script with this command:

   ```shell
   python solve.py
   ```

   The script will prompt you for input. After you enter the input, it will process it and print the output.

5. **Exit the Docker container**

   After you're done with the Python script, you can exit the Docker container by typing `exit` at the bash prompt and pressing enter, or by pressing `Ctrl+D`.
